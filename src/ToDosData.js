const ToDosData = [
    {
        id: 0,
        description: 'Сходить в магазин',
        completed: false
    },
{
    id: 1,
        description: 'Выкинуть мусор',
    completed: false
},
    {
        id: 2,
        description: 'Убрать в квартире',
        completed: false
    },
    {
        id: 3,
        description: 'Сварить борщ',
        completed: false
    },
    {
        id: 4,
        description: 'Написать программу',
        completed: false
    }
];
export default ToDosData;