import React, {Component} from 'react';
import './App.css';
import ToDoItem from './ToDoItem/ToDoItem.js';

import ToDosData from "./ToDosData";


class App extends Component {
    constructor() {
        super();
        this.state = {
            todoItems: ToDosData
        }
    }

    handleChange = id => {
        const index = this.state.todoItems.map(item => item.id).indexOf(id);

        let {todoItems} = this.state;
        todoItems[index].completed = true;

        this.setState({
            todoItems: todoItems
        });
    };

    render() {
        const {todoItems} = this.state;
        const activeTasks = todoItems.filter(task => task.completed === false);
        const completedTasks = todoItems.filter(task => task.completed === true);
        const finalTasks = [...activeTasks, ...completedTasks].map(item => {

            return (
                <ToDoItem
                    key={item.id}
                    description={item.description}
                    completed={item.completed}
                    handleChange={() => {
                        this.handleChange(item.id)
                    }}
                />
            )

        })

        return (
            <div className="App">
                {finalTasks}
            </div>
        );
    }
}

export default App;
