import React from 'react';

class ToDoItem extends React.Component{
    render() {
        return (
            <div className="todo-item">
                <div className="description-wrapper">
                    <p>{this.props.description}</p>
                </div>
                <div className="input-wrapper">
                    <input
                        type="checkbox"
                        defaultChecked={ this.props.completed }
                        onChange={ () => {this.props.handleChange()} }
                    />
                </div>
            </div>

        )
    }
}
export default ToDoItem;